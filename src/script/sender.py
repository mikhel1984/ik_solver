#!/usr/bin/env python

import rospy
#from geometry_msgs.msg import Pose, PoseArray
from ik_solver.msg import PoseStateArray
from ik_solver.msg import PoseState


#pub = rospy.Publisher('/iiwa/cmd/NewTrajectory', PoseArray, queue_size=1)
pub = rospy.Publisher('/cmd/Trajectory', PoseStateArray, queue_size=1)
rospy.init_node('publish_positions',anonymous=True)
rate = rospy.Rate(5)

msg = PoseStateArray()
msg.robot = "UR10e"
#msg.header.frame_id = '/iiwa'
#msg.header.stamp = rospy.Time.now()
#
'''
m1 = PoseState()
m1.position.x = 530
m1.position.y = 150
m1.position.z = 400
m1.orientation.x = 0
m1.orientation.y = -1
m1.orientation.z = 0
m1.orientation.w = 0
m1.gripperOpen = True
msg.poses.append(m1)
#
m2 = PoseState()
m2.position.x = 730
m2.position.y = 150
m2.position.z = 400
m2.orientation.x = 0
m2.orientation.y = -1
m2.orientation.z = 0
m2.orientation.w = 0
m2.gripperOpen = False
msg.poses.append(m2)
#
m3 = PoseState()
m3.position.x = 730
m3.position.y = -150
m3.position.z = 400
m3.orientation.x = 0
m3.orientation.y = -1
m3.orientation.z = 0
m3.orientation.w = 0
m3.gripperOpen = True
msg.poses.append(m3)
#
m4 = PoseState()
m4.position.x = 530
m4.position.y = -150
m4.position.z = 400
m4.orientation.x = 0
m4.orientation.y = -1
m4.orientation.z = 0
m4.orientation.w = 0
m4.gripperOpen = False
msg.poses.append(m4)
'''
m1 = PoseState()
m1.position.x = 606
m1.position.y = -300 
m1.position.z = 500
m1.orientation.x = -1
m1.orientation.y = 0
m1.orientation.z = -0.04
m1.orientation.w = 0
m1.gripperOpen = True
msg.poses.append(m1)
#
m2 = PoseState()
m2.position.x = 609
m2.position.y = 292
m2.position.z = 500
m2.orientation.x = -1
m2.orientation.y = 0
m2.orientation.z = -0.02 
m2.orientation.w = 0
m2.gripperOpen = False
msg.poses.append(m2)

#pub.publish(msg)
rate.sleep()
pub.publish(msg)
