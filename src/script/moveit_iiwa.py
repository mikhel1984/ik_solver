#!/usr/bin/env python

import sys
import rospy
import moveit_commander

from ik_solver.msg import PoseState, PoseStateArray
from trajectory_msgs.msg import JointTrajectory
from geometry_msgs.msg import Pose

class MoveItIiwaInterface(object):
  
  def __init__(self):
    "Iiwa node constructor"
    super(MoveItIiwaInterface, self).__init__()
    # initialize
    moveit_commander.roscpp_initialize(sys.argv)
    # init node
    rospy.init_node('FollowPoints',anonymous=True)
    self.pub = rospy.Publisher('/calc/Trajectory', JointTrajectory, queue_size=1)
    rospy.Subscriber('/cmd/Trajectory', PoseStateArray, self.callback)
    # robot interface
    self.robot = moveit_commander.RobotCommander()
    # joint group
    self.group = moveit_commander.MoveGroupCommander("manipulator")
    # trajectory points
    self.trajectory = []
    self.gripper = []
    # new trajectory
    self.isNew = False
    # names 
    self.names = ["j1","j2","j3","j4","j5","j6","j7","gripper"]
    self.robotName = "IIWA"
    #
    # TODO: set initial position (move or emulate...)
    print "MoveIt! interface is prepared"
  
  def evalAndPublish(self):
    "Define trajectory points and publish result"
    if not self.isNew:
      return True
    isNew = False
    # find way points
    STEP = 0.02    # m, cartesian distance
    (plan,fraction) = self.group.compute_cartesian_path(self.trajectory,STEP,0.0)
    print "Solvable part: ", fraction
    # TODO: emulate motion
    # modify commands
    traj = plan.joint_trajectory
    
    # ...
    self.pub.publish(traj)
    
  def callback(self,data):
    "Listen commands"
    if data.robot != self.robotName:
      return
    # clear
    self.isNew = False
    self.trajectory = []
    self.gripper = []
    # convert formats
    for pose in data.poses:
      p = Pose()
      p.position = pose.position
      p.orientation = pose.orientation
      gr = 1.0 if pose.gripperOpen else 0.0
      # duplicate points when gripper change state
      if len(self.gripper) > 0 and gr != self.gripper[-1]:
        self.trajectory.append(p)
        self.gripper.append(self.gripper[-1])  
      # new point    
      self.trajectory.append(p)
      self.gripper.append(gr)
    self.isNew = True
      
      
      